const en = require('./en.json');
const zh = require('./zh.json');
module.exports = {
  en: en,
  zh: zh,
};
